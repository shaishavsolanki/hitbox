//
//  GameScene.swift
//  PHysicsDemo
//
//  Created by Parrot on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let circle = SKSpriteNode(imageNamed: "circle")
    let square = SKSpriteNode(imageNamed: "square")
    let triangle = SKSpriteNode(imageNamed: "triangle")
    
    override func didMove(to view: SKView) {
        circle.position = CGPoint(x:self.size.width*0.25, y:self.size.height/2)
        square.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        triangle.position = CGPoint(x:self.size.width*0.75, y:self.size.height/2)
        
        addChild(circle)
        addChild(square)
        addChild(triangle)
    }
}
